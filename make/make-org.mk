# -*- coding: utf-8 -*-

# Config details
rootdir = $(shell pwd)

ifdef VERSION
	FULL_VERSION = $(VERSION)
else
	FULL_VERSION = dirty
endif

ORG_FILES = $(shell find -name "*\.org" -print)
OBJ_FILES = $(ORG_FILES:%.org=%-$(VERSION).html) \
	$(ORG_FILES:%.org=%-$(FULL_VERSION).tex) \
	$(ORG_FILES:%.org=%-$(FULL_VERSION).pdf)
DIRS := $(sort $(dir $(OBJ_FILES)))

.PHONY:
all: $(OBJ_FILES)

%-$(FULL_VERSION).html: %.org | $(DIRS)
	emacs $(notdir $<) --batch --chdir $(dir $*) -f org-html-export-to-html --kill
	mv $*.html $@

%-$(FULL_VERSION).tex: %.tex | $(DIRS)
	mv $*.tex $@

%.pdf: %.tex | $(DIRS)
	@echo "BUILDING $@"
	cd $(dir $<) && latexmk -f -xelatex -pdf $(notdir $<) && cd $(rootdir)

%.tex: %.org
	emacs $(notdir $<) --batch --chdir $(dir $*) -f org-latex-export-to-latex --kill

$(DIRS):
	mkdir -p $@

