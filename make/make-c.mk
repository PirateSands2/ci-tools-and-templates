# -*- coding: utf-8 -*-

## USER-CONFIGURATION--------------------------------------------------

# Project details
NAME = 
VERSION = 

# Compiler - currently only clang supported
CC = clang

# Make shell
SHELL = /bin/bash

# Directory setup
SRCDIR := src
BASE_BUILD_DIR := build
TEST_DIR := test

# Build flags
release_CFLAGS := -O3 -march=native -mavx2
release_ASFLAGS :=
release_LDFLAGS := 

test_CFLAGS := -O0 -fprofile-instr-generate -fcoverage-mapping -ferror-limit=10000 -g
test_ASFLAGS :=
test_LDFLAGS := -lm

VALGRIND_FLAGS := --leak-check=full --track-origins=yes --read-var-info=yes -s

## INTERNAL-RULES------------------------------------------------------

# Configure build info
STYLE = $(firstword $(MAKECMDGOALS))
ifeq ($(STYLE), check)
STYLE = test
else ifeq ($(STYLE), run)
STYLE = release
else ifeq ($(STYLE), test)
STYLE = test
else ifeq ($(STYLE), coverage)
STYLE = test
endif

BUILD_DIR := $(STYLE)-build

BINARY_NAME = $(BUILD_DIR)/$(NAME)-$(VERSION).elf
TEST_BINARY_NAME = $(BUILD_DIR)/test-$(NAME)-$(VERSION).elf

ifneq ($(STYLE), clean)
.PHONY: $(STYLE)
$(STYLE): $(BINARY_NAME) $(TEST_BINARY_NAME)
endif

# Utility rules
.PHONY: clean
clean:
	@echo Cleaning up
	@rm -rf *$(BASE_BUILD_DIR)

.PHONY: check
check: $(TEST_BINARY_NAME)
	@echo Running Tests
	@./$(TEST_BINARY_NAME)

.PHONY: run
run: $(BINARY_NAME)
	@printf "Running build ------------------------------>\n\n"
	@./$(BINARY_NAME)

.PHONY: coverage
coverage: $(BUILD_DIR)/coverage.txt $(BUILD_DIR)/coverage-lines.txt
	@echo Running coverage report

.PHONY: valgrind
valgrind: $(BINARY_NAME)
	@valgrind $(VALGRIND_FLAGS) $<

# Dependency flags
DEPFLAGS = -MT $@ -MMD -MP -MF $(BUILD_DIR)/$*.d

# Source and intermediate files
MAIN_FILE_OBJECT := $(BUILD_DIR)/$(SRCDIR)/main.c.o
STATIC_LIBS := $(shell find $(LIB_DIR) -name "*.a" -print)
CFILES := $(shell find $(SRCDIR) -name "*.c" -print)
ASFILES := $(shell find $(SRCDIR) -name "*.s" -print)

OBJFILES := $(CFILES:%.c=$(BUILD_DIR)/%.c.o) \
			$(ASFILES:%.s=$(BUILD_DIR)/%.s.o)
DEPFILES := $(CFILES:%.c=$(BUILD_DIR)/%.d) $(ASFILES:%.s=$(BUILD_DIR)/%.d)

CHECKFILES = $(shell find $(TEST_DIR) -name "*.tst" -print)

DIRS := $(sort $(dir $(OBJFILES)) $(dir $(CHECKFILES)))

# Binary build rules
$(BINARY_NAME): $(OBJFILES)
	@echo Building $@
	@$(CC) $($(STYLE)_LDFLAGS) $($(STYLE)_CFLAGS) -o $@ $^ $(STATIC_LIBS)

# Rules for building files
%.o: %.c
$(BUILD_DIR)/%.c.o: %.c | $(DIRS)
	@echo Building $<
	@$(CC) $(DEPFLAGS) $($(STYLE)_CFLAGS) -o $@ -c $<

$(BUILD_DIR)/%.s.o: %.s | $(DIRS)
	@echo Building $<
	@$(CC) $(DEPFLAGS) $($(STYLE)_ASFLAGS) -o $@ -c $<

# Rules for building test files
$(TEST_BINARY_NAME): $(filter-out $(MAIN_FILE_OBJECT),$(OBJFILES)) $(BUILD_DIR)/check.c
	@echo Building $@
	@$(CC) $($(STYLE)_LDFLAGS) $($(STYLE)_CFLAGS) -o $@ $^ $(STATIC_LIBS) -lpthread -lsubunit -lcheck -lrt -lm

$(BUILD_DIR)/check.c: $(CHECKFILES)
	@echo Building checkfile
	@checkmk $^ > $@

# Rules for building test coverage reports
$(BUILD_DIR)/coverage/coverage.profraw: $(TEST_BINARY_NAME)
	@echo Generating raw profiling data
	@LLVM_PROFILE_FILE=$@ ./$< > /dev/null

$(BUILD_DIR)/coverage/coverage.prodata: $(BUILD_DIR)/coverage/coverage.profraw
	@echo Processing profiling data
	@llvm-profdata merge $< -o $@

$(BUILD_DIR)/coverage.txt: $(BUILD_DIR)/coverage/coverage.prodata
	@echo Generating coverage report
	@llvm-cov report ./$(TEST_BINARY_NAME) -instr-profile=$< > $@

$(BUILD_DIR)/coverage-lines.txt: $(BUILD_DIR)/coverage/coverage.prodata
	@echo Generating line report
	@llvm-cov show ./$(TEST_BINARY_NAME) -instr-profile=$< > $@

# Ensure directories exist
$(DIRS):
	@mkdir -p $@

# Include external dependency information
$(DEPFILES):
include $(wildcard $(DEPFILES))
