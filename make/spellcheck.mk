# -*- coding: utf-8 -*-

LANGUAGE = en-gb
BUILD_DIR = reports

TXT_FILES = $(shell find -name "*\.org" -print)
REPORTS = $(TXT_FILES:%.org=$(BUILD_DIR)/%-report.txt)
STYLE_REPORTS = $(REPORTS:%-report.txt=%-style.txt)

DIRS := $(sort $(dir $(REPORTS)))

.PHONY:
grammar: $(REPORTS)

.PHONY:
style: $(STYLE_REPORTS)

$(BUILD_DIR)/%-report.txt: %.org | $(DIRS)
	java -jar /LanguageTool-5.0/languagetool-commandline.jar -l $(LANGUAGE) > $@

$(BUILD_DIR)/%-style.txt: %.org | $(DIRS)
	proselint $< > $@

$(DIRS):
	mkdir -p $@
